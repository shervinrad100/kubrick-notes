# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 01:59:58 2020

@author: sherv
"""
# TODO:
# single v multi
# multiplayer check, what if they enter invalid player? will default to player 2 choosing 

from Cartrige_class import * 

class GuessingGame(Cartrige):
    single_mode = ['1','1v1','single']
    
    def __init__(self):
        
        self.guesses = 0
        self._on = True
   
                
    def __main__(self):
        Break = False
      
        while self._on: 
            # single or multiplayer?
            while True:
                mode = input('Single or Multiplayer? (single|1|one,multiplayer|multi|2) ').strip().lower()
                if re.search('^(m|t|2)',mode):
                    mode = 'Multi'
                    break
                elif re.search('^(s|o|1)',mode) or mode in self.single_mode:
                    break
                elif mode in self.cmd:
                    confirm = input('Do you want to quit? (yes/no) ')
                    if confirm in self.cmd2:
                            self._on = False
                            Break = True
                            break
                    elif confirm in self.cmd:
                        pass
                    else:
                        print('Invalid Input')
                else:
                    print('Invalid input')
            
            if Break:
                break
                
            # select number of guesses
            self.select_n()
            if self.Break:
                break
             
            
            if mode == 'Multi':
                
                # who is guessing the word?
                while True:
                    player = input('Which player is guessing the number? (1/2) ').strip().lower()
                    if re.search('^(1|o)',player):
                        player = 1
                        break
                    elif re.search('^(2|t)',player):
                        player = 2
                        break
                    elif player in self.cmd:
                        confirm = input('Do you want to quit? (yes/no) ')
                        if confirm in self.cmd2:
                            self._on = False
                            Break = True
                            break
                        elif confirm in self.cmd:
                            pass
                        else:
                            print('Invalid Input')
                    else:
                        print('Enter valid player')                    

                if Break:
                    break
                
                # select number to be guessed
                while True:
                    if player == 1:
                        print('Player2 enter the number between 0 and 50:')
                    else:
                        print('Player1 enter the number between 0 and 50:')
                        
                    self.ans = getpass.getpass()
                    try:
                        assert int(self.ans)
                        break
                    except ValueError:
                        if self.ans in self.cmd:
                            confirm = input('Do you want to quit? (yes/no) ')
                            if confirm in self.cmd2:
                                    self._on = False
                                    Break = True
                                    break
                            elif confirm in self.cmd:
                                pass
                            else:
                                print('Invalid Input')
                        else:
                            print('Enter a valid integer or quit command')
                if Break:
                    break
                
                # win/loss conditions and score amendment
                self.play(Multiplayer=True, person=player)
            
            # single player: set random number between 0 and 50
            else:
                self.ans = random.randint(0,50)
                self.play()
    
    def select_n(self):
        self.Break = False
        while True:
            self.n = input("Enter number of guesses: ")
            try:
                assert int(self.n)
                break
            except ValueError:
                if self.n in self.cmd:
                    confirm = input('Do you want to quit? (yes/no) ')
                    if confirm in self.cmd2:
                        self._on = False
                        self.Break = True
                        break
                    elif confirm in self.cmd:
                        pass
                    else:
                        print('Invalid Input')
                else:
                    print('Enter a valid integer or quit command')
                    
    def play(self, Multiplayer=False, person=None):
        Break = False
        while self.guesses <= int(self.n):
            while True:
                guess = input('Enter your guess number: ')
                try:
                    assert int(guess)
                    break
                except ValueError:
                    if guess in self.cmd:
                        confirm = input('Do you want to quit? ')
                        if confirm in self.cmd2:
                            self._on = False
                            Break = True
                            break
                        elif confirm in self.cmd:
                            pass
                        else:
                            print('Invalid Input')
                    else:
                        print('Enter a valid integer or quit command')
                
            if Break:
                break
                
            self.guesses += 1 
            if int(guess) == int(self.ans):
                print('Thats correct!')
                self._on = False
                if Multiplayer:
                    self._is_winner(person)
                break
            elif int(guess) != int(self.ans) and self.guesses == int(self.n):
                print('Nope. You lose :(')
                self._on = False
                break
            else:
                if int(guess) > int(self.ans):
                    print('Nope. Go lower. Keep guessing...')
                elif int(guess) < int(self.ans):
                    print('Nope. Go higher. Keep guessing...')
            
            
    def _is_winner(self, person):
        self.player_won(person)