# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 02:01:48 2020

@author: sherv
"""

# TODO:
# add help to explain the game
# data validation for multi or single player
# multiplayer check, what if they enter invalid player? will default to player 2 choosing 
# data validation for word entry what if they enter int?
    # split() for item in lst if type(item) == 'int' raise ValueError try except block


from Cartrige_class import *

class Hangman(Cartrige):
    # init game mode options 
    single_mode = ['1','1v1','single']
    
    def __init__(self):
        # initialise super class runtime variable
        super().__init__()
        
        # allow player to choose game mode
        self.mode = input('Single or Multiplayer? ').strip().lower()
        if re.search('^m',self.mode) or self.mode not in self.single_mode:
            self.mode = 'Multi'
        
        # initialise game graphics
        self.a_hanged_man = ['|----------'
,'|         |  '
,'|        (X) '
,'|        /|\\'
,'|         |  '
,'|        / \\'
,'|____________']
        
        
        
        
    def __main__(self):
        
        if self.mode == 'Multi':
            # who is guessing the word? #####################data validation needed
            player = input('Which player is guessing the word? (1/2) ').strip().lower()
            if re.search('^(1|o)',player):
                player = 1
            else:
                player = 2
            
            # enter the word
            if player == 1:
                print('Player2 enter the word:')
            else:
                print('Player1 enter the word:')
            
            self.word = getpass.getpass().lower()
            self.play()
            
            if self.__win():
                self.player_won(player) 
        # if playing against computer, computer will pick a word at random    
        else:
            self.word = random.sample(words.words(),1)[0].lower()
            self.play()

        
    def play(self):
        
        # initialise game variables
        self.word_space = ['_' for _ in range(len(self.word))]
        self.lives = 7 
        self.indexed_word = {index:letter for index,letter in enumerate(self.word)}
        self.guessed = []
        
        while self._run:
            print(' '.join(self.word_space))
            
            guess = input('Enter your guess: ').strip().lower() ## assert len = 1
            
            try:
                assert len(guess) == 1
                assert type(guess) == 'str'
            except AssertionError:
                while len(guess) != 1 and type(guess) != 'str':
                    if guess in self.cmd:
                        self._quit()
                        break
                    else:
                        guess = input('Enter a letter as your guess: ').strip().lower()
                
            if guess not in self.word and guess not in self.guessed:
                self.lives -= 1
                self.print_the_man(self.lives)
                self.guessed.append(guess)
            elif guess in self.word and guess not in self.guessed:
                self.guessed.append(guess)
                for index,letter in self.indexed_word.items():
                    if letter == guess:
                        self.word_space[index] = guess
            elif guess in self.guessed:
                print('You have already guessed this')
                print('')
            else:
                print('Invalid input')
                        
            if self.__win():
                print('')
                print('##### YOU WIN! #####')
                print('')
                print('The word is', ''.join(self.word_space).upper())
                break
                
            if self.lives == 0:
                print('')
                print('##### You have been hanged #####')
                print('')
                print(f'The answer is: {self.word}')
                break
                
    
    def print_the_man(self,life):
        print(f'lives: {self.lives}')
        print('\n'.join(self.a_hanged_man[self.lives:]))
            
    def __win(self):
        if '_' in self.word_space:
            return False
        return True
    
