# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 01:58:30 2020

@author: sherv
"""

# TODO:
    # user regex to recognise game name or recognise main menu 
# create reset scores method
# add guessing game
# add connect 4
    

import re
import random
import getpass
from nltk.corpus import words
from Cartrige_class import *
from Hangman_class import *
from XnOs_class import *
from GuessingGame_class import *

class Cartrige():
       
    
    # allow player commands
    cmd = ['quit','q', 'break', 'exit', 'n', 'no']
    cmd2 = ['yes', 'y', 'start']
    games = ['XnOs', 'Hangman','GuessingGame']
    score = {"Player1":0, "Player2":0}
    
    print("For game help and functionalities type 'help'")
    print("To quit type: ")
    print(cmd)
    
    def __init__(self):
        self._run = True
        
        
    
    def __main__(self):
        while self._run:
            print("\n")
            print('######### Main menu #########')
            print('scores:',self.score)
            
            # print list of games available to choose from 
            print('Choose game\n',self.games)

            # prompt player to select a game
            game = input()
                
            # game selection and confirmation to start
            
            #XnOs
            if re.search("^(X|x)", game):
                start = input(f'Start XnOs? (yes/no) ').strip().lower() 
                if start in self.cmd2:
                    XnOs().__main__()
                elif start in self.cmd:
                    pass
                else:
                    print('Unrecognisable input')

            # Hangman
            elif re.search("^(h|H)(a|A)", game):
                start = input(f'Start Hangman? (yes/no) ').strip().lower() 
                if start in self.cmd2:
                    Hangman().__main__()
                elif start in self.cmd:
                    pass
                else:
                    print('Unrecognisable input')
            
            #GuessingGame
            elif re.search("^(g|G)", game):
                start = input(f'Start GuessTheNumber? (yes/no) ').strip().lower() 
                if start in self.cmd2:
                    GuessingGame().__main__()
                elif start in self.cmd:
                    pass
                else:
                    print('Unrecognisable input')

            # help
            elif re.search("^(h|H)(e|E)", game):
                need_help = input("Need help? (yes/no) ")
                if need_help in self.cmd2:
                    self._help()
                elif need_help in self.cmd:
                    pass
                else:
                    print('Unrecognisable input')
                    
            # Quit 
            elif game in self.cmd:
                Break = input(f'Shutdown Cartrige? (yes/no) ').strip().lower()
                if Break in self.cmd2:
                    break
                elif Break in self.cmd:
                    pass
                else:
                    print('Unrecognisable input')
            
            elif re.search('^re',game):
                self.reset_score()
                continue
                
            else:
                print("For game help and functionalities type 'help'")
                print("To quit type: ")
                print(self.cmd)
                
                
                
    def _quit(self):
        confirm = input('Are you sure you want to quit? (yes/no) ')
        if confirm in self.cmd2:
            self._run = False
        else:
            print('Invalid input')
        
    def _help(self):
        string = '''
        Welcome to my game. The idea was to create a console Cartrige like that of Atari.
        Each cartrige has a few games. 
        You play the games with your friends and the cartrige keeps track of the scores. 
        I have tried to add as much freedom to the player as possible to if you mistype something the game wont break!
        You can quit any time using some keywords. *All inputs are case insensitive.*
        I have implemented regex to recognise the name of the game when you start on main menu.
        If you can think of new cool functionalities let me know and I'll add them in!
        Type "reset" to reset the scores
        '''
        print(string)
        
    def player_won(self, player):
        self.score["Player%i" %player] += 1
        
    def reset_score(self):
        self.score = {"Player1":0, "Player2":0}
        print("scores reset")
        

Cartrige().__main__()
        
